package com.news.newstest.common

const val BASE_URL = "https://newsapi.org/v2/"
const val API_KEY = "74ee2136ff30492684fa31ecfac180e9"

const val QUERY_TOP_HEADLINES = "top-headlines"
const val QUERY_API_KEY = "apiKey"
const val QUERY_COUNTRY = "country"
const val QUERY_CATEGORY = "category"
const val COUNTRY_US = "us"
const val CATEGORY_BUSINESS = "business"
const val CATEGORY_SPORTS = "sports"
const val DATE_FORMAT = "dd.MM.yyyy HH:mm"
const val EMPTY_STRING = ""