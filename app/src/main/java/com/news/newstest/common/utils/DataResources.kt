package com.news.newstest.common.utils

data class DataResources<DATA_TYPE>(val data: DATA_TYPE? = null, val error: String? = null) {
    fun isSuccess() = error == null
}