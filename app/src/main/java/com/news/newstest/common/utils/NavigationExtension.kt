package com.news.newstest.common.utils

import android.view.View
import androidx.navigation.Navigation

fun View.navigateView(action: Int) = Navigation.findNavController(this).navigate(action)