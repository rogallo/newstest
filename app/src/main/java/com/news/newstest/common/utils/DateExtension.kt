package com.news.newstest.common.utils

import com.news.newstest.common.DATE_FORMAT
import com.news.newstest.common.EMPTY_STRING
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun String.convertDate() : String {
    val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
    return if (this.isEmpty()) EMPTY_STRING
    else formatter.format(LocalDateTime.parse(this, DateTimeFormatter.ISO_DATE_TIME))
}