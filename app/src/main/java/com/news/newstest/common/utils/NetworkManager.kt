package com.news.newstest.common.utils

import android.net.ConnectivityManager
import android.net.NetworkCapabilities

open class NetworkManager(private var connectivityManager: ConnectivityManager) {

    fun isConnected() = connectivityManager.run {
        getNetworkCapabilities(activeNetwork)?.run {
            hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
        } ?: false
    }
}