package com.news.newstest.common.utils

import android.app.Activity
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.news.newstest.R

fun Activity.showNewsDialog(title: String? = null, message: String? = null) {
    MaterialAlertDialogBuilder(this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(resources.getString(R.string.ok_button))
        { dialog, which -> dialog.dismiss()}
        .show()
}

fun ImageView.showImage(view: View, url: String?) {
    Glide.with(view).load(url)
        .centerCrop()
        .placeholder(R.drawable.image_placeholder)
        .into(this)
}

fun View.showSnackbar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}