package com.news.newstest.common.utils

import android.content.res.Resources
import com.news.newstest.R

class ErrorHandler(private val resources: Resources) {

    fun getErrorMessage(code: Int) = when(code) {
        in ERROR_VALUE_400..ERROR_VALUE_499 -> resources.getString(R.string.error_account)
        in ERROR_VALUE_500..ERROR_VALUE_599 -> resources.getString(R.string.error_server)
        else -> resources.getString(R.string.error_unknown)
    }

    fun getErrorForNetwork() = resources.getString(R.string.error_network)
}

private const val ERROR_VALUE_400 = 400
private const val ERROR_VALUE_499 = 499
private const val ERROR_VALUE_500 = 500
private const val ERROR_VALUE_599 = 599