package com.news.newstest.common.utils

import android.view.View
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import com.news.newstest.presentation.news.model.NewsResponseState

@BindingAdapter("progressVisibility")
fun ProgressBar.setProgressVisibility(responseState: NewsResponseState) {
    visibility = if (responseState is NewsResponseState.Loading) View.VISIBLE else View.GONE
}