package com.news.newstest.domain.model

import com.news.newstest.data.model.NewsModelDTO
import java.util.*

class NewsModelMapper {

    fun mapNewsFromServerToArticleList(newsModelDTO: NewsModelDTO?) = LinkedList<ArticleModel>().apply {
        newsModelDTO?.articles?.forEach { add(
            ArticleModel(
                source = it.source.source,
                title = it.title,
                description = it.description,
                urlToImage = it.urlToImage,
                publishedAt = it.publishedAt
            )
        ) }
    }
}