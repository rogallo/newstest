package com.news.newstest.domain.repository

import com.news.newstest.domain.model.AboutModel

interface AboutRepository {

    fun getAbout(): AboutModel
}