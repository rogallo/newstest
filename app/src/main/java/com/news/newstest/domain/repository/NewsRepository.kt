package com.news.newstest.domain.repository

import com.news.newstest.common.utils.DataResources
import com.news.newstest.domain.model.ArticleModel
import java.util.*

interface NewsRepository {

    suspend fun getNews(): DataResources<LinkedList<ArticleModel>>
}