package com.news.newstest.domain.usecase

import com.news.newstest.domain.repository.NewsRepository

open class GetNewsUseCase(private val newsRepository: NewsRepository) {

    suspend operator fun invoke() = newsRepository.getNews()
}