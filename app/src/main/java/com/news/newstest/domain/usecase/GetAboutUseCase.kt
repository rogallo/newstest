package com.news.newstest.domain.usecase

import com.news.newstest.domain.repository.AboutRepository

class GetAboutUseCase(private val aboutRepository: AboutRepository) {

    fun getAboutData() = aboutRepository.getAbout()
}