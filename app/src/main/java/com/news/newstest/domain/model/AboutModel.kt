package com.news.newstest.domain.model

data class AboutModel(val author: String, val apiSource: String)