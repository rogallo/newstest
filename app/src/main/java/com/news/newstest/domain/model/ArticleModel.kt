package com.news.newstest.domain.model

data class ArticleModel(val source: String?,
                        val title: String?,
                        val description: String?,
                        val urlToImage: String?,
                        val publishedAt: String?)