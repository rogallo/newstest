package com.news.newstest.data.model

import com.google.gson.annotations.SerializedName

data class ArticleModelDTO(
    @SerializedName(MODEL_SOURCE) val source: SourceModelDTO,
    @SerializedName(MODEL_TITLE) val title: String,
    @SerializedName(MODEL_DESCRIPTION) val description: String,
    @SerializedName(MODEL_IMAGE) val urlToImage: String,
    @SerializedName(MODEL_DATE) val publishedAt: String
)

const val MODEL_SOURCE = "source"
const val MODEL_TITLE = "title"
const val MODEL_DESCRIPTION = "description"
const val MODEL_IMAGE = "urlToImage"
const val MODEL_DATE = "publishedAt"