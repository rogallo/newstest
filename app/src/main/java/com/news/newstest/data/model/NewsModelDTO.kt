package com.news.newstest.data.model

import com.google.gson.annotations.SerializedName

data class NewsModelDTO(
    @SerializedName(MODEL_STATUS) val status: String,
    @SerializedName(MODEL_ARTICLES) val articles: List<ArticleModelDTO>
)

const val MODEL_STATUS = "status"
const val MODEL_ARTICLES = "articles"