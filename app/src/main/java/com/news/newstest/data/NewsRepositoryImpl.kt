package com.news.newstest.data

import com.news.newstest.common.API_KEY
import com.news.newstest.common.CATEGORY_BUSINESS
import com.news.newstest.common.COUNTRY_US
import com.news.newstest.common.utils.DataResources
import com.news.newstest.common.utils.ErrorHandler
import com.news.newstest.common.utils.NetworkManager
import com.news.newstest.domain.model.ArticleModel
import com.news.newstest.domain.model.NewsModelMapper
import com.news.newstest.domain.repository.NewsRepository
import java.util.*

open class NewsRepositoryImpl(
    private val newsApi: NewsApi,
    private val newsModelMapper: NewsModelMapper,
    private val errorHandler: ErrorHandler,
    private val networkManager: NetworkManager
): NewsRepository {

    override suspend fun getNews(): DataResources<LinkedList<ArticleModel>> {
        return if (!networkManager.isConnected()) {
            DataResources(error = errorHandler.getErrorForNetwork())
        } else {
            newsApi.getNews(API_KEY, COUNTRY_US, CATEGORY_BUSINESS).run {
                if (isSuccessful) {
                    DataResources(data = newsModelMapper.mapNewsFromServerToArticleList(body()))
                } else {
                    DataResources(error = errorHandler.getErrorMessage(code()))
                }
            }
        }
    }
}