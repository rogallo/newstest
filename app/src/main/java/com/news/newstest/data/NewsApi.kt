package com.news.newstest.data

import com.news.newstest.common.QUERY_API_KEY
import com.news.newstest.common.QUERY_CATEGORY
import com.news.newstest.common.QUERY_COUNTRY
import com.news.newstest.common.QUERY_TOP_HEADLINES
import com.news.newstest.data.model.NewsModelDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

  @GET(QUERY_TOP_HEADLINES)
  suspend fun getNews(@Query(QUERY_API_KEY) apiKey: String,
              @Query(QUERY_COUNTRY) country: String,
              @Query(QUERY_CATEGORY) category: String): Response<NewsModelDTO>
}