package com.news.newstest.data

import com.news.newstest.domain.model.AboutModel
import com.news.newstest.domain.repository.AboutRepository

class AboutRepositoryImpl : AboutRepository {

    override fun getAbout() = AboutModel(author = AUTHOR, apiSource = SOURCE)
}

private const val AUTHOR = "PR"
private const val SOURCE = "newsapi.org"