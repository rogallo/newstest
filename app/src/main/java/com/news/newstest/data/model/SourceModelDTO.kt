package com.news.newstest.data.model

import com.google.gson.annotations.SerializedName

data class SourceModelDTO(
    @SerializedName(MODEL_SOURCE_NAME) val source: String
)

const val MODEL_SOURCE_NAME = "name"