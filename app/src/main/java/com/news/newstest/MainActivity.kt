package com.news.newstest

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.drawer_layout as drawerLayout
import kotlinx.android.synthetic.main.activity_main.navigation_view as navigationView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var navigationController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initNavigation()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        menuItem.isChecked = true
        drawerLayout.closeDrawers()
        when(menuItem.itemId) {
            R.id.menu_news -> navigationController.navigate(R.id.newsFragment)
            R.id.menu_about -> navigationController.navigate(R.id.aboutFragment)
        }
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navigationController, drawerLayout)
    }

    override fun onBackPressed() {
        drawerLayout.run {
            if (isDrawerOpen(GravityCompat.START)) closeDrawer(GravityCompat.START)
            else super.onBackPressed()
        }
    }

    private fun initNavigation() {
        navigationController = Navigation.findNavController(this, R.id.nav_host_fragment)
        setSupportActionBar(toolbar)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowCustomEnabled(true)
        }
        NavigationUI.setupActionBarWithNavController(this, navigationController, drawerLayout)
        NavigationUI.setupWithNavController(navigationView, navigationController)
        navigationView.setNavigationItemSelectedListener(this)
    }
}
