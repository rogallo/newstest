package com.news.newstest.presentation.news.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.news.newstest.presentation.news.model.NewsResponseState
import com.news.newstest.common.utils.showNewsDialog
import com.news.newstest.common.utils.showSnackbar
import com.news.newstest.databinding.FragmentNewsBinding
import com.news.newstest.presentation.news.adapter.NewsAdapter
import com.news.newstest.presentation.news.viewmodel.NewsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.fragment_news.view.news_recyclerview as newsRecyclerView

class NewsFragment : Fragment() {

    private val newsAdapter by lazy { NewsAdapter() }
    private val newsViewModel: NewsViewModel by viewModel()
    private lateinit var viewDataBinding: ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentNewsBinding.inflate(inflater, container, false).apply {
        vm = newsViewModel
        viewDataBinding = this
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        if (savedInstanceState == null) newsViewModel.getNewsFromBusiness()
        initStateObserver()
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
    }

    private fun initView(view: View) {
        newsAdapter.run {
            view.newsRecyclerView.adapter = this
            clickAction = { title, message -> activity?.showNewsDialog(title, message) }
        }
    }

    private fun showError(message: String) {
        view?.showSnackbar(message)
    }

    private fun initStateObserver() {
        newsViewModel.responseViewState.observe(viewLifecycleOwner, Observer {
            if (it is NewsResponseState.Error) showError(it.errorMessage)
        })
    }
}