package com.news.newstest.presentation.news.model

import com.news.newstest.domain.model.ArticleModel
import java.util.*

sealed class NewsResponseState {
    object Loading : NewsResponseState()
    data class Success(val content: LinkedList<ArticleModel>) : NewsResponseState()
    data class Error(val errorMessage: String) : NewsResponseState()
}