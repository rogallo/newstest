package com.news.newstest.presentation.about.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.news.newstest.domain.usecase.GetAboutUseCase

class AboutViewModel(
    private val getAboutUseCase: GetAboutUseCase) : ViewModel() {

    private val mutableScreenState = MutableLiveData(ScreenState())
    val screenState: LiveData<ScreenState> by lazy { mutableScreenState }

    fun getAboutData() {
        getAboutUseCase.getAboutData().run {
            mutableScreenState.value =
                mutableScreenState.value?.copy(authorData = author, apiSourceData = apiSource)
        }
    }

    data class ScreenState(
        val authorData: String? = null,
        val apiSourceData: String? = null
    )
}