package com.news.newstest.presentation.about.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.news.newstest.databinding.FragmentAboutBinding
import com.news.newstest.presentation.about.viewmodel.AboutViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AboutFragment : Fragment() {

    private val aboutViewModel: AboutViewModel by viewModel()
    private lateinit var viewDataBinding: ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentAboutBinding.inflate(inflater, container, false).apply {
        vm = aboutViewModel
        viewDataBinding = this
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        aboutViewModel.getAboutData()
    }
}