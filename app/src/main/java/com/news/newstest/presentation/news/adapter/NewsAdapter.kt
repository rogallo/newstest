package com.news.newstest.presentation.news.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.news.newstest.R
import com.news.newstest.presentation.news.model.NewsResponseState
import com.news.newstest.domain.model.ArticleModel
import com.news.newstest.presentation.news.adapter.viewholder.NewsViewHolder
import java.util.*

class NewsAdapter : RecyclerView.Adapter<NewsViewHolder>() {

    private val items = LinkedList<ArticleModel>()
    var clickAction: (String?, String?) -> Unit = { s: String?, s1: String? -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        NewsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.view_news_item, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.run {
            bind(items[position])
            itemView.setOnClickListener {
                clickAction(items[position].title, items[position].description) }
        }
    }

    fun setItems(articleList: LinkedList<ArticleModel>) {
        items.run {
            clear()
            addAll(articleList)
        }
        notifyDataSetChanged()
    }
}

@BindingAdapter("newsData")
fun RecyclerView.setNewsData(data: NewsResponseState) {
    (adapter as NewsAdapter).run {
        if (data is NewsResponseState.Success) setItems(data.content) }
}