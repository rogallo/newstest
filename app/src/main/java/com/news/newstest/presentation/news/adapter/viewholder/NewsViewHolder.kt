package com.news.newstest.presentation.news.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.news.newstest.common.utils.convertDate
import com.news.newstest.common.utils.showImage
import com.news.newstest.domain.model.ArticleModel
import kotlinx.android.synthetic.main.view_news_item.view.item_news_date as newsDate
import kotlinx.android.synthetic.main.view_news_item.view.item_news_description as newsDescription
import kotlinx.android.synthetic.main.view_news_item.view.item_news_image as newsImage
import kotlinx.android.synthetic.main.view_news_item.view.item_news_title as newsTitle

class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(articleModel: ArticleModel) = with(articleModel) {
        itemView.run {
            newsTitle.text = title
            newsDescription.text = description
            newsDate.text = publishedAt?.convertDate()
            newsImage.showImage(this, urlToImage)
        }
    }
}