package com.news.newstest.presentation.news.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.news.newstest.domain.usecase.GetNewsUseCase
import com.news.newstest.presentation.news.model.CoroutineContextProvider
import com.news.newstest.presentation.news.model.NewsResponseState
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NewsViewModel(
    private val getNewsUseCase: GetNewsUseCase,
    private val coroutineContextProvider: CoroutineContextProvider) : ViewModel() {

    private val responseStateLiveData = MutableLiveData<NewsResponseState>()
    val responseViewState: LiveData<NewsResponseState> by lazy { responseStateLiveData }

    private val handler = CoroutineExceptionHandler { _, exception ->
        responseStateLiveData.value = NewsResponseState.Error(exception.message.toString())
    }

    fun getNewsFromBusiness() {
        responseStateLiveData.value = NewsResponseState.Loading
        viewModelScope.launch(handler) {
            val data = withContext(coroutineContextProvider.io) { getNewsUseCase.invoke() }
            responseStateLiveData.value =
                if (data.isSuccess()) NewsResponseState.Success(data.data!!)
                else NewsResponseState.Error(data.error.toString())
        }
    }
}