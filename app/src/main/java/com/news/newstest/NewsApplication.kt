package com.news.newstest

import android.app.Application
import com.news.newstest.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class NewsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@NewsApplication)
            modules(listOf(networkModule, viewModelModule, systemModule, useCaseModule, injectModule))
        }
    }
}