package com.news.newstest.di

import android.content.Context
import android.net.ConnectivityManager
import com.news.newstest.common.utils.NetworkManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val systemModule = module {

    factory { androidContext().resources }

    factory { androidContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }

    factory { NetworkManager(get()) }
}