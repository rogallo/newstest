package com.news.newstest.di

import com.news.newstest.common.utils.ErrorHandler
import com.news.newstest.data.AboutRepositoryImpl
import com.news.newstest.data.NewsRepositoryImpl
import com.news.newstest.domain.model.NewsModelMapper
import com.news.newstest.domain.usecase.GetAboutUseCase
import com.news.newstest.domain.usecase.GetNewsUseCase
import com.news.newstest.presentation.about.viewmodel.AboutViewModel
import com.news.newstest.presentation.news.model.CoroutineContextProvider
import com.news.newstest.presentation.news.viewmodel.NewsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { NewsViewModel(get(), get()) }
    viewModel { AboutViewModel(get()) }
}

val useCaseModule = module {
    factory { provideGetNewsUseCase(get()) }
    factory { provideGetAboutUseCase(get()) }
}

val injectModule = module {
    factory { NewsModelMapper() }
    factory { ErrorHandler(get()) }
    factory { CoroutineContextProvider() }
}

private fun provideGetNewsUseCase(newsRepositoryImpl: NewsRepositoryImpl) =
    GetNewsUseCase(newsRepositoryImpl)

private fun provideGetAboutUseCase(aboutRepositoryImpl: AboutRepositoryImpl) =
    GetAboutUseCase(aboutRepositoryImpl)