package com.news.newstest

import com.news.newstest.common.utils.convertDate
import org.junit.Assert.assertEquals
import org.junit.Test

class DateExtensionUnitTest {

    @Test
    fun test_validateDateConvert() {
        assertEquals("2020-12-28T01:40:00Z".convertDate(), "28.12.2020 01:40")
    }

    @Test
    fun test_validateEmptyDateConvert() {
        assertEquals("".convertDate(), "")
    }
}