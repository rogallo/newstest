package com.news.newstest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.news.newstest.common.utils.DataResources
import com.news.newstest.data.NewsRepositoryImpl
import com.news.newstest.domain.model.ArticleModel
import com.news.newstest.domain.usecase.GetNewsUseCase
import com.news.newstest.model.TestCoroutineContextProvider
import com.news.newstest.presentation.news.model.NewsResponseState
import com.news.newstest.presentation.news.viewmodel.NewsViewModel
import com.news.newstest.rule.CoroutineRule
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class RequestDataUnitTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var testCoroutineRule = CoroutineRule()

    private lateinit var newsViewModel: NewsViewModel

    private lateinit var getNewsUseCase: GetNewsUseCase

    @Mock
    private lateinit var newsRepository: NewsRepositoryImpl

    @Mock
    private lateinit var viewStateObserver: Observer<NewsResponseState>

    @Mock
    private val list = LinkedList<ArticleModel>()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getNewsUseCase = GetNewsUseCase(newsRepository)
        newsViewModel = NewsViewModel(getNewsUseCase, TestCoroutineContextProvider()).apply {
            responseViewState.observeForever(viewStateObserver)
        }
    }

    @Test
    fun test_request_success() {
        testCoroutineRule.runBlockingTest {
            val data = DataResources(data = list)
            whenever(getNewsUseCase.invoke()).thenReturn(data)
            newsViewModel.getNewsFromBusiness()
            verify(viewStateObserver).onChanged(NewsResponseState.Loading)
            verify(viewStateObserver).onChanged(NewsResponseState.Success(data.data!!))
        }
    }

    @Test
    fun test_request_main_error() {
        testCoroutineRule.runBlockingTest {
            val error = Error()
            whenever(getNewsUseCase.invoke()).thenThrow(error)
            newsViewModel.getNewsFromBusiness()
            verify(viewStateObserver).onChanged(NewsResponseState.Loading)
            verify(viewStateObserver).onChanged(NewsResponseState.Error(error.message.toString()))
        }
    }

    @Test
    fun test_request_network_error() {
        testCoroutineRule.runBlockingTest {
            val data = DataResources<LinkedList<ArticleModel>>(data = null, error = "Something went wrong with network connection")
            initEnvironmentForReturnDataWithError(data)
            var errorMessage = (newsViewModel.responseViewState.value as NewsResponseState.Error).errorMessage
            assertEquals(errorMessage, "Something went wrong with network connection")
        }
    }

    @Test
    fun test_request_account_error() {
        testCoroutineRule.runBlockingTest {
            val data = DataResources<LinkedList<ArticleModel>>(data = null, error = "Something went wrong with your account")
            initEnvironmentForReturnDataWithError(data)
            var errorMessage = (newsViewModel.responseViewState.value as NewsResponseState.Error).errorMessage
            assertEquals(errorMessage, "Something went wrong with your account")
        }
    }

    @Test
    fun test_request_server_error() {
        testCoroutineRule.runBlockingTest {
            val data = DataResources<LinkedList<ArticleModel>>(data = null, error = "Something went wrong with server")
            initEnvironmentForReturnDataWithError(data)
            var errorMessage = (newsViewModel.responseViewState.value as NewsResponseState.Error).errorMessage
            assertEquals(errorMessage, "Something went wrong with server")
        }
    }

    @Test
    fun test_request_unknown_error() {
        testCoroutineRule.runBlockingTest {
            val data = DataResources<LinkedList<ArticleModel>>(data = null, error = "Something went wrong")
            initEnvironmentForReturnDataWithError(data)
            var errorMessage = (newsViewModel.responseViewState.value as NewsResponseState.Error).errorMessage
            assertEquals(errorMessage, "Something went wrong")
        }
    }

    private suspend fun initEnvironmentForReturnDataWithError(data: DataResources<LinkedList<ArticleModel>>) {
        whenever(getNewsUseCase.invoke()).thenReturn(data)
        newsViewModel.getNewsFromBusiness()
        verify(viewStateObserver).onChanged(NewsResponseState.Loading)
        verify(viewStateObserver).onChanged(NewsResponseState.Error(data.error.toString()))
    }
}