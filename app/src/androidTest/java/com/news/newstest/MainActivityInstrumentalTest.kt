package com.news.newstest

import android.view.Gravity
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.hamcrest.CoreMatchers.not
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityInstrumentalTest {

    @Test
    fun test_toolbar_visibility() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.toolbar)).check(matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_nav_host_visibility() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.nav_host_fragment)).check(matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_navigation_view_visibility() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.drawer_layout))
            .check(matches(isClosed(Gravity.LEFT)))
            .perform(DrawerActions.open())
        onView(withId(R.id.navigation_view)).check(matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_navigation_view_invisible() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.navigation_view)).check(matches(not(ViewMatchers.isDisplayed())))
    }
}