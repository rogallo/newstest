package com.news.newstest

import android.content.Context
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.news.newstest.common.utils.ErrorHandler
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

class ErrorHandlerInstrumentalTest {

    private lateinit var errorHandler: ErrorHandler

    @Before
    fun setup() {
        errorHandler = ErrorHandler(getApplicationContext<Context>().resources)
    }

    @Test
    fun text_account_error() {
        assertEquals(errorHandler.getErrorMessage(400), "Something went wrong with your account")
    }

    @Test
    fun text_server_error() {
        assertEquals(errorHandler.getErrorMessage(500), "Something went wrong with server")
    }

    @Test
    fun text_unknown_error() {
        assertEquals(errorHandler.getErrorMessage(600), "Something went wrong")
    }

    @Test
    fun text_network_error() {
        assertEquals(errorHandler.getErrorForNetwork(), "Something went wrong with network connection")
    }
}